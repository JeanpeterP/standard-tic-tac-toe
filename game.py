def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

def game_over(board, winner):
    print("GAME OVER")
    print_board(board)
    print(winner, "has won")
    exit()

def is_row_winner(board):
    winner = False
    if board[0] == board[1] and board[1] == board[2]:
        winner = True
    if board[3] == board[4] and board[4] == board[5]:
        winner = True
    if board[6] == board[7] and board[7] == board[8]:
        winner = True
    return winner

def is_column_winner(board):
    winner = False
    if board[0] == board[3] and board[3] == board[6]:
        winner = True
    if board[1] == board[4] and board[4] == board[7]:
        winner = True
    if board[2] == board[5] and board[5] == board[8]:
        winner = True
    return winner

def is_diagnonal_winner(board):
    winner = False
    if board[0] == board[4] and board[4] == board[8]:
        winner = True
    if board[2] == board[4] and board[4] == board[6]:
        winner = True
    return winner

board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
current_player = "X"

for move_number in range(1, 10):
    print_board(board)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    board[space_number] = current_player

    if is_row_winner(board):
        game_over(board, board[0])
    elif is_row_winner(board):
        game_over(board, board[3])
    elif is_row_winner(board):
        game_over(board, board[6])
    elif is_column_winner(board):
        game_over(board, board[0])
    elif is_column_winner(board):
        game_over(board, board[1])
    elif is_column_winner(board):
        game_over(board, board[2])
    elif is_diagnonal_winner(board):
        game_over(board, board[0])
    elif is_diagnonal_winner(board):
        game_over(board, board[2])

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
